<?php
/**
 * @file
 * Twitter Lite module for creating two blocks of twitter feeds.
 */

/**
 * Implementation of hook_menu().
 */
function twitter_lite_menu() {
  $items = array();
  $items['admin/config/content/twitter_lite'] = array(
    'title' => 'Twitter Lite',
    'description' => 'Configuration for Twitter feed blocks.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('twitter_lite_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implementation of hook_form().
 */
function twitter_lite_form($form, &$form_state) {
	
  $form = array();

  $form['twitter_lite_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Username'),
    '#default_value' => variable_get('twitter_lite_username'),
    '#size' => 100,
    '#maxlength' => 100,
    '#description' => t('The username that is used on the twitter account. Without the @ symbol.'),
    '#required' => FALSE,
  );

  $form['twitter_lite_search_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Search Term'),
    '#default_value' => variable_get('twitter_lite_search_term'),
    '#size' => 100,
    '#maxlength' => 100,
    '#description' => t('Enter a search term for this field. You can use a hashtag or terms.'),
    '#required' => FALSE,
  );

  $form['twitter_lite_tweets_number_block_1'] = array(
    '#type' => 'select',
    '#title' => t('Number of tweets to display: User Feed'),
    '#default_value' => variable_get('twitter_lite_tweets_number_block_1'),
    '#options' =>  drupal_map_assoc(range(1, 10)),
    '#maxlength' => 2,
    '#required' => FALSE,
  );
  $form['twitter_lite_tweets_number_block_2'] = array(
    '#type' => 'select',
    '#title' => t('Number of tweets to display: Search Term'),
    '#default_value' => variable_get('twitter_lite_tweets_number_block_2'),
    '#options' =>  drupal_map_assoc(range(1, 10)),
    '#maxlength' => 2,
    '#required' => FALSE,
  );
  $form['twitter_lite_tweets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter API'),
    '#description' => t('If you don\'t have an API key, please go to this URL to get one: <a href="https://apps.twitter.com/" target="_blank">https://apps.twitter.com/</a>'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['twitter_lite_tweets']['twitter_lite_oauth'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Access Token'),
    '#default_value' => variable_get('twitter_lite_oauth'),
    '#required' => FALSE,
  );
  $form['twitter_lite_tweets']['twitter_lite_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Access Token Secret'),
    '#default_value' => variable_get('twitter_lite_secret'),
    '#required' => FALSE,
  );
  $form['twitter_lite_tweets']['twitter_lite_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer Key'),
    '#default_value' => variable_get('twitter_lite_key'),
    '#required' => FALSE,
  );
  $form['twitter_lite_tweets']['twitter_lite_consumer'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer Secret'),
    '#default_value' => variable_get('twitter_lite_consumer'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Twitter API block.
 */
function twitter_lite_1() {

  $token = variable_get('twitter_lite_oauth');
  $token_secret = variable_get('twitter_lite_secret');
  $consumer_key = variable_get('twitter_lite_key');
  $consumer_secret = variable_get('twitter_lite_consumer');

  $host = 'api.twitter.com';
  $method = 'GET';
  $path = '/1.1/statuses/user_timeline.json';
  $username = variable_get('twitter_lite_username');
  
  $query = array(
    'screen_name' => $username,
    'count' => variable_get('twitter_lite_tweets_number_block_1'),
  );

  $oauth = array(
    'oauth_consumer_key' => $consumer_key,
    'oauth_token' => $token,
    'oauth_nonce' => (string)mt_rand(),
    'oauth_timestamp' => time(),
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_version' => '1.0'
  );

  $oauth = array_map("rawurlencode", $oauth); 
  $query = array_map("rawurlencode", $query);

  $arr = array_merge($oauth, $query);
  asort($arr);
  ksort($arr);
  $querystring = urldecode(http_build_query($arr, '', '&'));

  $url = "https://$host$path";

  $base_string = $method . "&" . rawurlencode($url) . "&" . rawurlencode($querystring);

  $key = rawurlencode($consumer_secret) . "&" . rawurlencode($token_secret);

  $signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, true)));

  $url .= "?" . http_build_query($query);

  $oauth['oauth_signature'] = $signature;
  ksort($oauth);

  function add_quotes($str) { 
	  return '"' . $str . '"';
	}
	
  $oauth = array_map("add_quotes", $oauth);

  $auth = "OAuth " . urldecode(http_build_query($oauth, '', ', '));

  $options = array(CURLOPT_HTTPHEADER => array("Authorization: $auth"),
    CURLOPT_HEADER => false,
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false);

  $feed = curl_init();
  curl_setopt_array($feed, $options);
  $json = curl_exec($feed);
  curl_close($feed);

  $twitter_data = json_decode($json);

  // Create clickable links within the tweet.
  function linkify_tweet($twitter_data) {

    // Convert urls to <a> links.
    $twitter_data = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $twitter_data);

    // Convert hashtags to twitter searches in <a> links.
    $twitter_data = preg_replace("/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $twitter_data);

    // Convert attags to twitter profiles in <a> links.
    $twitter_data = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a target=\"_blank\" href=\"http://www.twitter.com/$1\">@$1</a>", $twitter_data);

    return $twitter_data;

  }

  if(isset($twitter_data->errors[0]->code)) {
    $error = $twitter_data->errors[0]->code;
  }
  else {
    $error = '';
  }

  if (empty($twitter_data)) {
    drupal_set_message(t('Please check the username you\'ve entered.'), 'error');
  }
  elseif ($error == 215 || $error == 34 || $error == 32) {
    drupal_set_message(t('Please check the Twitter username you\'ve entered.'), 'error');
  }
  elseif (isset($twitter_data->error) and $twitter_data->error == 'Not authorized.') {
    drupal_set_message(t('The twitter account that you\'ve entered has been blocked or suspended.'), 'error');
  }
  else {
    $tweets = $twitter_data;

    $content = "<div class=\"twitter-wrap\">";
      $content .= '<ul class="twitter-list">';
      foreach ($tweets as $tweet) {
        $content .= '<li class="row">';
        $content .= '<div class="tweet-logo"><img src="' . $tweet->user->profile_image_url . '"></div>';
        $content .= '<div class="tweet-text">' . linkify_tweet($tweet->text) . '';
        $content .= '<div class="intents">';
        $content .= '<span class="reply"><a target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=' . $tweet->id . '">Reply</a></span>';
        $content .= '<span class="retweet"><a target="_blank" href="https://twitter.com/intent/retweet?tweet_id=' . $tweet->id . '">Retweet</a></span>';
        $content .= '<span class="favorite"><a target="_blank" href="https://twitter.com/intent/favorite?tweet_id=' . $tweet->id .'">Favorite</a></span>';
        $content .= '</div></div>';
        $content .= '</li>';
      }
      $content .= '</ul>';
    $content .= '</div>';

    return $content;
  }

}


/**
 * Twitter API Search results block.
 */
function twitter_lite_search() {

  require_once('TwitterAPIExchange.php');

  $token2 = variable_get('twitter_lite_oauth');
  $token_secret2 = variable_get('twitter_lite_secret');
  $consumer_key2 = variable_get('twitter_lite_key');
  $consumer_secret2 = variable_get('twitter_lite_consumer');

  $settings = array(
    'oauth_access_token' => $token2,
    'oauth_access_token_secret' => $token_secret2,
    'consumer_key' => $consumer_key2,
    'consumer_secret' => $consumer_secret2
  );

  $url = 'https://api.twitter.com/1.1/search/tweets.json';
  $search = variable_get('twitter_lite_search_term');
  $encoded = rawurlencode($search);
  $getfield = '?q=' . $encoded . '&count=' . variable_get('twitter_lite_tweets_number_block_2');
  $requestMethod = 'GET';

  $twitter = new TwitterLiteAPIExchange($settings);
  $response = $twitter->setGetfield($getfield)
    ->buildOauth($url, $requestMethod)
    ->performRequest();

  $twitter_data2 = json_decode($response);
  
  // Create clickable links within the tweet.
  function linkify_tweet2($twitter_data2) {

    // Convert urls to <a> links.
    $twitter_data2 = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $twitter_data2);

    // Convert hashtags to twitter searches in <a> links.
    $twitter_data2 = preg_replace("/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $twitter_data2);

    // Convert attags to twitter profiles in <a> links.
    $twitter_data2 = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a target=\"_blank\" href=\"http://www.twitter.com/$1\">@$1</a>", $twitter_data2);

    return $twitter_data2;

  }

  $tweets2 = $twitter_data2->statuses;

  if(empty($tweets2)) {
    drupal_set_message(t('There were no tweets found for the search term entered.'), 'error');
  }
   else {
    $content = "<div class=\"twitter-wrap\">";
      $content .= '<ul class="twitter-list">';
      foreach ($tweets2 as $tweet2) {
        $content .= '<li class="row">';
        $content .= '<div class="tweet-logo"><img src="' . $tweet2->user->profile_image_url . '"></div>';
        $content .= '<div class="tweet-text">' . linkify_tweet($tweet2->text) . '';
        $content .= '<div class="intents">';
        $content .= '<span class="reply"><a target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=' . $tweet2->id . '">Reply</a></span>';
        $content .= '<span class="retweet"><a target="_blank" href="https://twitter.com/intent/retweet?tweet_id=' . $tweet2->id . '">Retweet</a></span>';
        $content .= '<span class="favorite"><a target="_blank" href="https://twitter.com/intent/favorite?tweet_id=' . $tweet2->id .'">Favorite</a></span>';
        $content .= '</div></div>';
        $content .= '</li>';
      }
      $content .= '</ul>';
    $content .= '</div>';

    return $content;

  }
}


/**
 * Implements hook_block_info().
 */
function twitter_lite_block_info() {
  $blocks['twitter_lite_twitter_block_1'] = array(
    'info' => t('Tweets - User Feed'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['twitter_lite_twitter_block_2'] = array(
    'info' => t('Tweets - Search Results'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function twitter_lite_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'twitter_lite_twitter_block_1':
      $block['subject'] = t('Tweets - User Feed');
      $block['content'] = twitter_lite_1();
    break;

    case 'twitter_lite_twitter_block_2':
      $block['subject'] = t('Tweets - Search Results');
      $block['content'] = twitter_lite_search();
    break;
  }
return $block;
}