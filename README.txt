CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
 
INTRODUCTION
------------

This module was created out of the need for a basic and easy to setup twitter 
feed. The Twitter module is great for extending the Twitter API into your site 
with great features.

However, I wanted a lightweight Twitter feed module that generates a block 
of feeds. The feeds block is

This module creates two Twitter feed blocks. You can also choose how many 
tweets to show per block.

UPDATE: 3/13/2015 I've updated this to include a block with a search tweet 
feed. It can use search terms or hashtags.

UPDATE: 7/21/2016 Revised API error notification. Began work on port to 
Drupal 8.


REQUIREMENTS
------------

You must have a twitter account to create the app information: 
https://apps.twitter.com/. 

You'll need to create an Access Token key, Consumer Key and Secret Key to use
this module with Twitter's API. Additionally, you must have a twitter user 
account if you would like to use the User Feed block.

No other Drupal modules are required to use this particular module.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
 * Create an app here: https://apps.twitter.com
 
 * Generate your access keys and tokens
 
 * Paste those in the settings page for Twitter Lite at 
   admin/config/content/twitter_lite in your site
 
 
CONFIGURATION
-------------

To use the User Feed block, you'll need to enter a twitter username. 

To use the search block, you can enter in any term or hash tag or phrase.

You can select the amount of tweets that you would like to show up per 
block on the settings page using the dropdown for the corresponding blocks.
   
   
   
MAINTAINERS
-----------
Current maintainers:
 * Jason Glisson (tk421jag) - https://www.drupal.org/u/tk421jag  
 